from django.db import models

class Post(models.Model):
    priority = models.CharField(max_length=100)
    person = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
