from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Post

class ToDoForm(forms.Form):
    priority =[("low", "Low"), ("medium", "Medium"), ("high", "High")]
    person =[("dad", "Dad"), ("mom", "Mom"), ("grandpa", "Grandpa")]
    status =[("not_started", "Not Started"), ("started", "Started"), ("finished", "Finished")]

    priority = forms.ChoiceField(choices=priority, label='Priority')
    person = forms.ChoiceField(choices=person, label='Person')
    status = forms.ChoiceField(choices=status, label='Status')
    description = forms.CharField(max_length=150, label='Description')

class ToDoEditForm(forms.Form):
    priority =[("low", "Low"), ("medium", "Medium"), ("high", "High")]
    person =[("dad", "Dad"), ("mom", "Mom"), ("grandpa", "Grandpa")]
    status =[("not_started", "Not Started"), ("started", "Started"), ("finished", "Finished")]

    priority = forms.ChoiceField(choices=priority, label='Priority')
    person = forms.ChoiceField(choices=person, label='Person')
    status = forms.ChoiceField(choices=status, label='Status')
    description = forms.CharField(max_length=150, label='Description')
    id = forms.CharField(max_length=999, label='id')

def saveTask(request):
         priority = request.POST.get('priority')
         person = request.POST.get('person')
         status = request.POST.get('status')
         description = request.POST.get('description')
         id = request.POST.get('id')

         Post.objects.filter(id=id).update(priority=priority)
         Post.objects.filter(id=id).update(person=person)
         Post.objects.filter(id=id).update(status=status)
         Post.objects.filter(id=id).update(description=description)

         form = ToDoForm()
         posts = Post.objects.all()
         return render(request, 'main/index.html', {'form': form, 'posts': posts})

def addTask(request):
         tdModel=Post()
         tdModel.priority=request.POST.get('priority')
         tdModel.person=request.POST.get('person')
         tdModel.status=request.POST.get('status')
         tdModel.description=request.POST.get('description')
         tdModel.save()

         form = ToDoForm()
         posts = Post.objects.all()
         return render(request, 'main/index.html', {'form': form, 'posts': posts})

def editTask(request):
            number = request.POST.get("edit")
            priority = list(Post.objects.values('priority').filter(id=number))
            person = list(Post.objects.values('person').filter(id=number))
            status = list(Post.objects.values('status').filter(id=number))
            description = list(Post.objects.values('description').filter(id=number))

            form = ToDoEditForm()

            args = {'form': form, 'posts': number, 'priority': priority, 'person': person, 'status': status, 'description': description}

            return render(request, 'main/edit.html/', args)

def deleteTask(request):
         number = request.POST.get("delete")
         Post.objects.filter(id=number).delete()
         form = ToDoForm()
         posts = Post.objects.all()
         return render(request, 'main/index.html', {'form': form, 'posts': posts})

def form(request):
     form = ToDoForm()
     posts = Post.objects.all()
     return render(request, 'main/index.html', {'form': form, 'posts': posts})

def filterTask(request):
            filter = request.POST.get("filter")
            words = filter.split("?")

            priority = words[1]
            person = words[2]
            status = words[3]

            if (priority == 'filter'):
                if(person == 'filter'):
                    if(status == 'filter'):
                        form = ToDoForm()
                        posts = Post.objects.all()
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                    else:
                        form = ToDoForm()
                        posts = Post.objects.all().filter(status=status)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                else:
                    if(status == 'filter'):
                        form = ToDoForm()
                        posts = Post.objects.all().filter(person=person)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                    else:
                        form = ToDoForm()
                        posts = Post.objects.all().filter(person=person, status=status)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
            else:
                if(person == 'filter'):
                    if(status == 'filter'):
                        form = ToDoForm()
                        posts = Post.objects.all().filter(priority=priority)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                    else:
                        form = ToDoForm()
                        posts = Post.objects.all().filter(priority=priority,status=status)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                else:
                    if(status == 'filter'):
                        form = ToDoForm()
                        posts = Post.objects.all().filter(priority=priority,person=person)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
                    else:
                        form = ToDoForm()
                        posts = Post.objects.all().filter(priority=priority,person=person,status=status)
                        return render(request, 'main/index.html', {'form': form, 'posts': posts})
