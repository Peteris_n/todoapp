from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return HttpResponse("Hello World!")

def site(response):
    return render(response, "main/index.html", {})
